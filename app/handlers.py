import logging

import webapp2
from webapp2_extras import jinja2
from jinja2 import TemplateNotFound


class BaseHandler(webapp2.RequestHandler):
    @webapp2.cached_property
    def jinja2(self):
        """Returns Jinja2 renderer cached in the app registry."""
        return jinja2.get_jinja2(app=self.app)

    def render_response(self, _template, **context):
        """Renders template and writes the result to the response."""
        rv = self.jinja2.render_template(_template, **context)
        self.response.write(rv)


class StaticPage(BaseHandler):
    """Takes pagename from url and treats it as a template name"""
    def get(self, pagename):
        if not pagename:
            pagename = self.app.config.get('index_template')
        try:
            self.render_response(pagename)
        except TemplateNotFound:
            self.abort(404)


#-----------------------------------------------------------------------------
# Error handlers
#-----------------------------------------------------------------------------
def handle_404(request, response, exception):
    logging.exception(exception)
    rv = jinja2.get_jinja2(app=request.app).render_template('404.html')
    response.write(rv)
    response.set_status(404)


def handle_500(request, response, exception):
    logging.exception(exception)
    rv = jinja2.get_jinja2(app=request.app).render_template('500.html')
    response.write(rv)
    response.set_status(500)
