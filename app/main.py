import webapp2
import jinja2
import os

jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))

routes = [
    (r'/(.*)', 'app.handlers.StaticPage'),
]

config = {
    'index_template': 'index.html'
}

app = webapp2.WSGIApplication(routes=routes, debug=True, config=config)
app.error_handlers[404] = 'app.handlers.handle_404'
app.error_handlers[500] = 'app.handlers.handle_500'
