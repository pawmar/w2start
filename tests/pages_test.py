import unittest2
from webtest import TestApp
from google.appengine.ext import testbed

from webapp2_extras import jinja2

from app.main import app as application


class BaseTest(unittest2.TestCase):
    """Base class for appengine tests"""
    def setUp(self):
        self.testbed = testbed.Testbed()
        self.testbed.activate()

        self.test_app = TestApp(application)

    def tearDown(self):
        self.testbed.deactivate()


class PageTest(BaseTest):
    """Test page loading"""
    def test_index(self):
        """Test index page"""
        response = self.test_app.get('/')
        self.assertEqual(response.status, '200 OK')

        index_template = application.config['index_template']
        rendered_template = jinja2.get_jinja2(app=application) \
                .render_template(index_template)
        self.assertEqual(response.body, rendered_template)

    def test_page(self):
        page = 'test.html'
        response = self.test_app.get('/' + page)
        self.assertEqual(response.status, '200 OK')

        rendered_template = jinja2.get_jinja2(app=application) \
                .render_template(page)
        self.assertEqual(response.body, rendered_template)

    def test_404(self):
        """Test 404 Not Found """
        response = self.test_app.get('/no-such-page', status='*')
        self.assertEqual(response.status, '404 Not Found')

        rendered_template = jinja2.get_jinja2(app=application) \
                .render_template('404.html')
        self.assertEqual(response.body, rendered_template)


